{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
module Application where

import Foundation
import Network.Wai
import Network.Wai.Handler.Warp
import Servant
import Settings
import System.Log.FastLogger


import Handler.Register
import Handler.Login

type BwhsAPI = "api" :> "accounts" :> "register" :> Post '[JSON] Int

bwhsApi :: Proxy BwhsAPI
bwhsApi = Proxy

--handlers :: AppEnv -> BwHandler AccountReq
handlers = registerHandler :<|> loginHandler

app :: AppEnv -> Application
app env = serve bwhsApi appWithReader
  where
    addReader = runReaderTNat env
    appWithReader = enter addReader handlers

appMain :: IO ()
appMain = do
  logger <- newStdoutLoggerSet 1
  let env = AppEnv { appSettings = Settings.defaultSettings
                   , appLogger = logger
                   , appDB = 0
                   }
  run 3000 $ app env

  
  -- TODO: Read environment variables
  -- TODO: Start listening

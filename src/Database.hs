module Database ( module Database
                , Connection
                , Pool
                ) where

import Data.Pool
import Data.Time (NominalDiffTime)
import Database.PostgreSQL.Simple

data DbConnectInfo = DbConnectInfo
  { dbConnInfo :: ConnectInfo
  , dbSubPools :: Int
  , dbNumConnections :: Int
  , dbMaxIdleTime :: NominalDiffTime
  }

defaultDbConnectInfo :: DbConnectInfo
defaultDbConnectInfo =
  DbConnectInfo { dbConnInfo = ConnectInfo { connectHost = "localhost"
                                           , connectPort = 5432
                                           , connectUser = "bwhs"
                                           , connectPassword = "bwhs"
                                           , connectDatabase = "bwhs"
                                           }
                , dbSubPools = 1
                , dbNumConnections = 10
                , dbMaxIdleTime = 15
                }

getPool :: DbConnectInfo -> IO (Pool Connection)
getPool connInfo =
  createPool (connect $ dbConnInfo connInfo) close subPools maxIdleTime numConnections
  where
    subPools = dbSubPools connInfo
    numConnections = dbNumConnections connInfo
    maxIdleTime = dbMaxIdleTime connInfo


{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE RankNTypes #-}

module Foundation where

--import Control.Monad.Base
--import Control.Monad.Except
--import Control.Monad.Logger
import Control.Monad.Reader
--import Control.Monad.Trans.Resource

import System.Log.FastLogger (LoggerSet)

--import qualified Servant
import Servant
--import qualified Servant

--import Database
import Settings

data AppEnv = AppEnv
  { appSettings :: Settings
  , appLogger :: LoggerSet
  , appDB :: Int
  }


type BwHandler = ReaderT AppEnv Handler




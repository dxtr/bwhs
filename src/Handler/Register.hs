{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DataKinds #-}

module Handler.Register where

import GHC.Generics (Generic)
import Data.Aeson
import Data.Text
import Servant

import Foundation
--import Database
--import Servant.Server

data AccountReq = AccountReq
  { accountreqName :: Maybe Text
  , accountreqEmail :: Text
  , accountreqPasswordHash :: Text
  , accountreqPasswordHint :: Maybe Text
  , accountreqKey :: Text
  } deriving (Show, Generic)

instance FromJSON AccountReq

type RegisterAPI = "api" :> "accounts" :> "register"
--                   :> ReqBody '[JSON] AccountReq
                   :> Post '[JSON] AccountReq

--registerHandler :: Maybe AccountReq -> BwHandler AccountReq
registerHandler _env = undefined

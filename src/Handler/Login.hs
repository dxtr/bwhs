{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DataKinds #-}


module Handler.Login where

import GHC.Generics (Generic)
import Data.Aeson

import Servant

data LoginReq = LoginReq
  { test :: Int
  } deriving (Show, Generic)

instance FromJSON LoginReq

type LoginAPI = "login" :>
              Get '[JSON] LoginReq

loginHandler _env = undefined

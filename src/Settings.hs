{-# LANGUAGE OverloadedStrings #-}
module Settings where

import Data.Text (Text)
import Data.Word (Word16)
import Control.Monad.Logger (LogLevel(..))
--import System.Log.FastLogger (LoggerSet)

import Database

data Settings = Settings
  { settingPort :: Word16 -- Listen port
  , settingHost :: Text -- Listen host
  , settingDbInfo :: DbConnectInfo
  , settingLogLevel :: LogLevel
  , settingLogBufSize :: Int
  , settingJwtSecret :: Int
  }

defaultSettings :: Settings
defaultSettings = Settings { settingPort = 3000
                           , settingHost = "localhost"
                           , settingDbInfo = defaultDbConnectInfo
                           , settingLogLevel = LevelDebug
                           , settingLogBufSize = 1
                           , settingJwtSecret = 0
                           }
